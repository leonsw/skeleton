<?php

declare(strict_types=1);
/**
 * This file is part of Leonsw.
 *
 * @link     https://leonsw.com
 * @document https://docs.leonsw.com
 * @contact  group@leonsw.com
 * @license  https://leonsw.com/LICENSE
 */
use Leonsw\Server\Server;

return [
    Server::HTTP_NAME => [
    ],
    Server::GRPC_NAME => [
    ],
];
