<?php

declare(strict_types=1);
/**
 * This file is part of Leonsw.
 *
 * @link     https://leonsw.com
 * @document https://docs.leonsw.com
 * @contact  group@leonsw.com
 * @license  https://leonsw.com/LICENSE
 */
return [
    'uri' => 'http://127.0.0.1:2379',
    'version' => 'v3beta',
    'options' => [
        'timeout' => 10,
    ],
];
